resource "oci_identity_idp_group_mapping" "test_idp_group_mapping" {
  #Required
    group_id             = var.oci_group_ocid
    idp_group_name       = var.idp_group_mapping_idp_group_name
    identity_provider_id = var.idp_ocid
}

resource "null_resource" "idcs_group_mapping" {

    triggers = {
        idcs_oci_v2_app_name = var.idcs_oci_v2_app_name
        idp_group_mapping_idp_group_name = var.idp_group_mapping_idp_group_name
    }
    provisioner "local-exec" {
        command  = "venv/bin/python main.py --config_file config.ini grant ${var.idcs_oci_v2_app_name} \"${var.idp_group_mapping_idp_group_name}\""
        working_dir = "/Users/gordon/PycharmProjects/IDCSTerraformShim/"
    }
    provisioner "local-exec" {
        when = destroy
        command  = "venv/bin/python main.py --config_file config.ini revoke  ${self.triggers.idcs_oci_v2_app_name} \"${self.triggers.idp_group_mapping_idp_group_name}\""
        working_dir = "/Users/gordon/PycharmProjects/IDCSTerraformShim/"
    }
}

data "oci_identity_idp_group_mappings" "test_idp_group_mappings" {
  #Required
    identity_provider_id = "ocid1.saml2idp.oc1..aaaaaaaavwzcjtq3atrzixwy2o6lsrwec5u3n2l6x5fadrnpea2oktjfd2ya"
}

output "idp_group_mappings" {
    value = data.oci_identity_idp_group_mappings.test_idp_group_mappings.idp_group_mappings
}
