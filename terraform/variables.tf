variable "region"{
    type = string
    default="us-ashburn-1"
}

variable "tenancy_ocid"{
    type = string
    default="ocid1.tenancy.oc1..aaaaaaaabpm2vrjioztrjhwxysfkbekblv3jqbgyxq7itbbort45qpefdfcq"
}

variable "user_ocid"{
    type = string
    default="ocid1.user.oc1..aaaaaaaa4kwickojkaxemi6elvyndmiqn5nqhudgk5hnh2poixex7nkfrymq"
}

variable "fingerprint"{
    type = string
    default="69:30:63:60:ea:e2:9b:4f:c8:c2:aa:a1:e0:ed:b8:7f"
}

variable "private_key_path"{
    default="~/.oci/active.pem"
}

variable "identity_provider_protocol" {
  default = "SAML2"
}

variable "idp_group_mapping_idp_group_name" {
  default = "Analytics Admin"
}

variable "idcs_oci_v2_app_name" {
    default ="OCI-V2-App-trevorrow"
}

variable "identity_provider_name" {
  default = "oracleidentitycloudservice"
}

variable "oci_group_ocid"{
  default= "ocid1.group.oc1..aaaaaaaa2ql6aigobtdbe5bgioprbzkkooleojnlnnnfvdeircjaag35szea"
}

variable "idp_ocid" {
  default = "ocid1.saml2idp.oc1..aaaaaaaavwzcjtq3atrzixwy2o6lsrwec5u3n2l6x5fadrnpea2oktjfd2ya"
}