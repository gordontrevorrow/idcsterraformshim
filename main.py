# Copyright (c) 2000, 2021, Oracle and/or its affiliates.
# The Universal Permissive License (UPL), Version 1.0
# Subject to the condition set forth below, permission is hereby granted to any
# person obtaining a copy of this software, associated documentation and/or data
# (collectively the "Software"), free of charge and under any and all copyright
# rights in the Software, and any and all patent rights owned or freely
# licensable by each licensor hereunder covering either (i) the unmodified
# Software as contributed to or provided by such licensor, or (ii) the Larger
# Works (as defined below), to deal in both
# (a) the Software, and
# (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if
# one is included with the Software (each a "Larger Work" to which the Software
# is contributed by such licensors),
# without restriction, including without limitation the rights to copy, create
# derivative works of, display, perform, and distribute the Software and make,
# use, sell, offer for sale, import, export, have made, and have sold the
# Software and the Larger Work(s), and to sublicense the foregoing rights on
# either these or other terms.
# This license is subject to the following condition:
# The above copyright notice and either this complete permission notice or at
# a minimum a reference to the UPL must be included in all copies or
# substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import base64
import configparser
import http.client
import json
import urllib


def build_scim_request_headers(access_token):
    headers = {
        'Authorization': f'Bearer {access_token}',
        'Content-Type': 'application/json'
    }
    return headers


def grant_group_to_managed_app(access_token, app_id, group_id, group_name, host):
    conn = http.client.HTTPSConnection("%s" % host)
    payload = json.dumps({
        "grantee": {
            "type": "Group",
            "value": f"{group_id}"
        },
        "app": {
            "value": f"{app_id}"
        },
        "grantedAttributeValuesJson": "{\"attributes\" : {\"idpGroups\":[\"+" + group_id + "/" + group_name + "\"]}}",
        "grantMechanism": "ADMINISTRATOR_TO_GROUP",
        "schemas": [
            "urn:ietf:params:scim:schemas:oracle:idcs:Grant"
        ]
    })
    headers = build_scim_request_headers(access_token)
    conn.request("POST", "/admin/v1/Grants", payload, headers)
    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))


def find_group_id(host, access_token, group_name):
    conn = http.client.HTTPSConnection(host)
    # conn.debuglevel = 1
    # logging.basicConfig()
    # logging.getLogger().setLevel(logging.DEBUG)
    # requests_log = logging.getLogger("requests.packages.urllib3")
    # requests_log.setLevel(logging.DEBUG)
    # requests_log.propagate = True
    payload = ''
    headers = build_scim_request_headers(access_token)
    filter = urllib.parse.quote("displayName eq \"%s\"" % group_name)
    url = "/admin/v1/Groups?filter=%s" % filter
    conn.request("GET", url, payload, headers)
    res = conn.getresponse()
    data = res.read()
    response = json.loads(data.decode("utf-8"))
    assert response["totalResults"] == 1
    return response["Resources"][0]["id"]


def find_app_id(host, access_token, app_name):
    conn = http.client.HTTPSConnection(host)
    payload = ''
    headers = build_scim_request_headers(access_token)
    filter = urllib.parse.quote("displayName eq \"%s\"" % app_name)
    conn.request("GET", "/admin/v1/Apps?filter=%s" % filter, payload, headers)
    res = conn.getresponse()
    data = res.read()
    response = json.loads(data.decode("utf-8"))
    assert response["totalResults"] == 1
    return response["Resources"][0]["id"]


def find_grant_id(host, access_token, group_id, app_id):
    conn = http.client.HTTPSConnection(host)
    payload = ''
    headers = build_scim_request_headers(access_token)
    filter = urllib.parse.quote("grantee[value eq \"%s\" and type eq \"Group\"] and app.value eq "
                                "\"%s\"" % (group_id, app_id))
    attributes = urllib.parse.quote("attributes=entitlement[attributeName eq \"appRoles\"].attributeValue")
    conn.request("GET",
                 "/admin/v1/Grants?filter=%s&%s" % (filter, attributes), payload, headers)
    res = conn.getresponse()
    data = res.read()
    response = json.loads(data.decode("utf-8"))
    assert response["totalResults"] == 1
    return response["Resources"][0]["id"]


def revoke_group_from_managed_app(access_token, grant_id, host):
    conn = http.client.HTTPSConnection(host)
    payload = ''
    headers = build_scim_request_headers(access_token)
    conn.request("DELETE", "/admin/v1/Grants/%s" % grant_id, payload, headers)
    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))


def get_access_token(host, client_id, client_secret):
    conn = http.client.HTTPSConnection(host)
    payload = f'grant_type=client_credentials&scope=urn%3Aopc%3Aidm%3A__myscopes__&authority={host}'
    b64String = f"{client_id}:{client_secret}"
    creds = base64.b64encode(b64String.encode("utf-8"))
    headers = {
        'Authorization': f'Basic {creds.decode("utf-8")}',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    conn.request("POST", "/oauth2/v1/token", payload, headers)
    res = conn.getresponse()
    data = json.loads(res.read().decode("utf-8"))
    return data['access_token']


def main(argv=None):
    parser = argparse.ArgumentParser(description='Grant, or revoke, an IDCS managed app to an IDCS group')
    parser.add_argument('operation', help='grant or revoke')
    parser.add_argument('app_name', help='Name of the IDCS managed app that is being granted')
    parser.add_argument('group_name', help='Name of the IDCS group the managed app is being grated to')
    parser.add_argument('--config_file', help="Path to the configuration file")
    parser.add_argument('--profile', help="Configuration profile that should be used", default="DEFAULT")
    args = parser.parse_args(argv)
    config = configparser.ConfigParser()
    config.read(args.config_file)
    profile = args.profile
    host = config[profile]["host"]
    client_id = config[profile]["client_id"]
    client_secret = config[profile]["client_secret"]
    admin_client_id = config[profile]["admin_client_id"]
    admin_client_secret = config[profile]["admin_client_secret"]

    if args.operation == "grant":
        at = get_access_token(host, admin_client_id, admin_client_secret)
        group_id = find_group_id(host, at, args.group_name)
        app_id = find_app_id(host, at, args.app_name)
        at = get_access_token(host, client_id, client_secret)
        grant_group_to_managed_app(at, app_id, group_id, args.group_name, host)
    if args.operation == "revoke":
        at = get_access_token(host, admin_client_id, admin_client_secret)
        group_id = find_group_id(host, at, args.group_name)
        app_id = find_app_id(host, at, args.app_name)
        grant_id = find_grant_id(host, at, group_id, app_id)
        at = get_access_token(host, client_id, client_secret)
        revoke_group_from_managed_app(at, grant_id, host)


if __name__ == '__main__':
    main()
